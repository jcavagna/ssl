<?
include 'models/views.php';
include 'models/checklogin.php';
include 'models/usersModel.php';

$views = new views();
$logins = new ckUser();
$userModel = new usersModel();
session_start();

$views->getView("views/incs/header.php");
$views->getView("views/incs/nav.php");

if(isset($_GET["action"])){
	$action = $_GET["action"];
}else{
	$action = "";
}

if($action==""){
	$data = "<a href='?action=userlogin'>LOGIN</a>";
	$views->getView("views/pages/content.php", $data);

// Login
}else if($action=="userlogin"){
		$data = $views->getView("views/incs/login.php");
}else if($action=="checklogin"){
	$data = array("un"=>$_POST["uname"],
					"pass"=>$_POST["pass"]);

	$test = $logins->checkUser($data);
	$msg = "Invalid Login";

	if($test == 1){
		$views->getView("views/pages/userpage.php");
		$data = $userModel->getUsers();
		$views->getView("views/bawdy.php", $data);
	}else{
		$views->getView("views/incs/login.php", $msg);
	}

// Update
}else if($_GET["action"]=="updateform"){
	$data = $userModel->getUser($_GET["uid"]);
	$views->getview("views/farm.php", $data);
}else if($_GET["action"]=="updateuser"){
	$userModel->updateUser($_POST["uid"], $_POST["uname"], $_POST["upass"]);

	$views->getView("views/pages/userpage.php");
	$users = $userModel->getUsers();
	$views->getView("views/bawdy.php", $users);

// Create
}else if($action=="createform"){
	$views->getView("views/cfarm.php");
}else if($action=="createuser"){
	$userModel->createUser($_POST["uname"], $_POST["upass"]);

	$views->getView("views/pages/userpage.php");
	$users = $userModel->getUsers();
	$views->getView("views/bawdy.php", $users);

// Delete
}else if($action=="deleteuser"){
	$data = $_GET['uid'];
	$userModel->deleteUser($data);

	$views->getView("views/pages/userpage.php");
	$users = $userModel->getUsers();
	$views->getView("views/bawdy.php", $users);

// Logout
}else if($action=="logout"){
	$_SESSSION["loggedin"] = 0;
	$data="<a href='?action=userlogin'>LOGIN</a>";
	$views->getView("views/pages/content.php", $data);
	session_destroy();

// Register
}else if($action=="registeruser"){
	$views->getView("views/rfarm.php");
}else if($action=="registercomplete"){
	$userModel->registerUser($_POST["uname"], $_POST["upass"]);
	$views->getView("views/pages/content.php");
}

$views->getView("views/incs/footer.php");

?>