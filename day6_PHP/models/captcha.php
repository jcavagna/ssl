<?
class captchasecurity {

	public function randchar($length = 8){
	    $str = null;
	    $minval = 1; 
	    $maxval = 3;
	    if (gettype($length) != 'integer'){
	        $length = 8;		
	    }
	    for ($i = 0;$i < $length;$i++) {
	    	
	        switch (@rand($minval, $maxval)) {
		        case 1: $str .= chr(rand(48, 57)); // 0-9
		            break;
		        case 2: $str .= chr(rand(97, 122)); // a-z
		            break;
		        case 3: $str .= chr(rand(65, 90)); // A-Z
		            break;
	    	}
	    }
	    return $str;

	}

	public function captchaBox($msg){
		$container = imagecreate(300,200);
		$black = imagecolorallocate($container, 0, 0, 0);
		$white = imagecolorallocate($container, 255, 255, 255);
		$font = 'img/Ubuntu-R.ttf';
		imagefilledrectangle($container, 0, 0, 250, 150, $black);
		imagerectangle($container, 10, 10, 290, 190, $white);
		imagefttext($container,32, 0, 50, 115, $white, $font, $msg);
		// header('Content-Type: image/png'); << get a warning from this code
		imagepng($container, 'img/captcha.png');
		imagedestroy($container);
	}
}
?>