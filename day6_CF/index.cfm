<cfset views = createObject("component", "models/views")>
<cfset users = createObject("component", "models/users")>
<cfset cap = createObject("component", "models/captcha")>
<cfset imga = createObject("component", "models/images")>

<cfset views.getView("../views/header.cfm")>
<cfset cap.cap("blahblah")>
<cfset data=users.getAll()>



<!--- php GET == url.	php POST == form. --->
<cfif isdefined("url.action")>

	<cfif url.action is "delete">
		<cfset users.deleteUser(url.id)>
		<cfset data=users.getAll()>
		<cfset views.getView("../views/body.cfm", data)>

	<cfelseif url.action is "addform">
		<cfset views.getView("../views/cform.cfm")>

	<cfelseif url.action is "adduser">
		<cfset users.addUser(form.name, form.pass)>
		<cfset data=users.getAll()>
		<cfset views.getView("../views/body.cfm", data)>

	<cfelseif url.action is "updateform">
		<cfset data = users.getOne(url.id)>
		<!---<cfdump var=#user#>--->
		<cfset views.getView("../views/uform.cfm", data)>

	<cfelseif url.action is "updateuser">
		<cfset users.updateUser(form.id, form.name, form.pass)>
		<cfset data=users.getAll()>
		<cfset views.getView("../views/body.cfm", data)>

	<cfelseif url.action is "upload">
		<cfset imga.up(form.upload)>
		<cfset myimf = ImageNew('/Applications/Railo/webapps/www/SSL/day6/img/front.jpg')>
		<cfset ImageResize(myimf, "120", "90", "blackman", 2)>

		<cfimage source="#myimf#" action="write" destination="/Applications/Railo/webapps/www/SSL/day6/img/test.jpeg" overwrite="true">
		
	</cfif>
<cfelse>
	<cfset data=users.getAll()>

	<cfset views.getView("../views/body.cfm", data)>

</cfif>

