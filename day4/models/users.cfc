<cfcomponent>
	
	<cffunction name="getAll" returntype="query">

		<cfquery datasource="ssl" name="users">
			select * from userTbl
		</cfquery>
		<cfreturn users>
	</cffunction>

	<cffunction name="getOne" returntype="query">

		<cfargument name="id" required="yes">
	
		<cfquery datasource="ssl" name="user">
			select * from userTbl where user_id = 
			<cfqueryparam value="#id#">
		</cfquery>

		<cfreturn user>
	</cffunction>

	<cffunction name="deleteUser" returntype="void">
		<cfargument name="id" required="yes">

		<cfquery datasource="ssl" name="users">
			delete from userTbl where user_id = <cfqueryparam value="#id#">
		</cfquery>
	</cffunction>

	<cffunction name="addUser" returntype="void">
		<cfargument name="name" required="yes">
		<cfargument name="pass" required="yes">

		<cfquery datasource="ssl" name="users">
			insert into userTbl (user_name, user_pass) 
			values (<cfqueryparam value="#name#">, <cfqueryparam value="#pass#">)
			
		</cfquery>
	</cffunction>

	<cffunction name="updateUser" returntype="void">
		<cfargument name="id" required="yes">
		<cfargument name="name" required="yes">
		<cfargument name="pass" required="yes">

		<cfquery datasource="ssl" name="users">
			update userTbl set user_name = <cfqueryparam value="#name#">, 
			user_pass = <cfqueryparam value="#pass#">	
			where user_id = <cfqueryparam value="#id#">
		</cfquery>
	</cffunction>

</cfcomponent>

