# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.29)
# Database: SSL1310
# Generation Time: 2013-10-10 19:03:22 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table emailTbl
# ------------------------------------------------------------

DROP TABLE IF EXISTS `emailTbl`;

CREATE TABLE `emailTbl` (
  `email_id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(20) unsigned NOT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`email_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `emailTbl_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `userTbl` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `emailTbl` WRITE;
/*!40000 ALTER TABLE `emailTbl` DISABLE KEYS */;

INSERT INTO `emailTbl` (`email_id`, `user_id`, `user_email`)
VALUES
	(1,2,'nothingforfree@yahoo.com');

/*!40000 ALTER TABLE `emailTbl` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table userTbl
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userTbl`;

CREATE TABLE `userTbl` (
  `user_id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  `user_pass` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `userTbl` WRITE;
/*!40000 ALTER TABLE `userTbl` DISABLE KEYS */;

INSERT INTO `userTbl` (`user_id`, `user_name`, `user_pass`)
VALUES
	(2,'Joseph','password123'),
	(5,'yep','newstuff'),
	(9,'whatthe','lolpassword'),
	(10,'whoami','idunno');

/*!40000 ALTER TABLE `userTbl` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
