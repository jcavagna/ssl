<? 
// controller (index)
// error_reporting("OFF");

include 'models/viewModel.php'; 

$viewModel = new viewModel();
// $data = array("name"=>"orcun");
$viewModel->getView("views/header.php");
$viewModel->getView("views/nav.php");

if(!empty($_GET["action"])){
	if($_GET["action"]=="form"){
		$viewModel->getView("views/login.php");
	}else if($_GET["action"]=="logg"){
		//var_dump($_GET);
		if(preg_match('/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/', $_POST["username"])){
			echo "Good Username";
		}else{
			echo "Bad Username";
		}
		if(preg_match('/^([a-zA-Z0-9@*#]{8,15})$/', $_POST["loginpassword"])){
			echo "Good Password";
		}else{
			echo "Bad Password";
		}
	}
}else{
	$viewModel->getView("views/login.php");
}

$viewModel->getView("views/body.php");

if(!empty($_GET["action"])){
	if($_GET["action"]=="form"){
		$viewModel->getView("views/quote.php");
	}else if($_GET["action"]=="process"){
		//var_dump($_GET);

		// First Name
		if(preg_match('/^[A-Z]?[-a-zA-Z]([a-zA-Z])*$/', $_POST["basic_firstname"])){
			echo "Good First Name";
		}else{
			echo "Bad First Name";
		}

		// Last Name
		if(preg_match('/^[A-Z]?[-a-zA-Z]([a-zA-Z])*$/', $_POST["basic_lastname"])){
			echo "Good Last Name";
		}else{
			echo "Bad Last Name";
		}

		// Company Pin
		if(preg_match('/^-?\d*?$/', $_POST["basic_pin"])){
			echo "Good PIN";
		}else{
			echo "Bad PIN";
		}

		// Email
		if(preg_match('/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/', $_POST["basic_email"])){
			echo "Good Email";
		}else{
			echo "Bad Email";
		}

		// Phone Number
		if(preg_match('/(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]‌​)\s*)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)([2-9]1[02-9]‌​|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})/', $_POST["basic_phone"])){
			echo "Good Phone";
		}else{
			echo "Bad Phone";
		}

		// Radio
		if(preg_match('/[email]|[phone]|[mail]/', $_POST["basic_pref"])){
			echo "Good Contact Pref";
		}else{
			echo "Bad Contact Pref";
		}

		// Drop Down
		if(preg_match('/[a-zA-Z]{2}/', $_POST["basic_state"])){
			echo "Good State Select";
		}else{
			echo "Bad State Select";
		}

		// URL
		if(preg_match('/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/', $_POST["site1"])){
			echo "Good URL";
		}else{
			echo "Bad URL";
		}

		// Check Box
		if(preg_match("/[a-zA-Z]/", $_POST["services_needed"])){
			echo "Good Services";
		}else{
			echo "Bad Services";
		}

		// Budget
		if(preg_match('/^\$?\d{1,3}([,]?\d{3})*(\.\d{0,2})?$/', $_POST["budget"])){
			echo "Good Budget";
		}else{
			echo "Bad Budget";
		}
	}
}else{
	$viewModel->getView("views/quote.php");
}
$viewModel->getView("views/footer.php");
// <cfdump var="">
?>
