<div id="quote_page">
<form action="?action=process" method="post" id="quoteform">
	<fieldset id="basic_info">
		<dl><legend>Basic Information</legend>
		<dt><label for="basic_firstname">First Name</label></dt>
		<dd><input type="text" class="tfi" name="basic_firstname" value="" id="basic_firstname" autofocus="autofocus" placeholder="John" required="required" /></dd>
		<dt><label for="basic_lastname">Last Name</label></dt>
		<dd><input type="text" class="tfi" name="basic_lastname" value="" id="basic_lastname" required="required" placeholder="Smith" /></dd>
		<dt><label for="basic_company">Company Name</label></dt>
		<dd><input type="text" class="tfi" name="basic_company" value="" id="basic_company" placeholder="Optional" /></dd>
		<dt><label for="basic_pin">Company PIN</label></dt>
		<dd><input type="text" class="tfi" name="basic_pin" value="" id="basic_pin" placeholder="Optional" /></dd></dl>
	</fieldset>
	<fieldset id="basic_contact">
		<dl><legend>Contact Information</legend>
		<dt><label for="basic_email">Email</label></dt>
		<dd><input type="email" class="tfi" name="basic_email" value="" id="basic_email" placeholder="id@domain.com" required="required" /></dd>
		<dt><label for="basic_phone">Phone Number</label></dt>
		<dd><input type="text" class="tfi" name="basic_phone" value="" id="basic_phone" placeholder="(xxx) xxx-xxxx" required="required" /></dd>
		<dt><label for="basic_street">Street Address</label></dt>
		<dd><input type="text" class="tfi" name="basic_street" value="" id="basic_street" required="required" /></dd>
		<dt><label for="basic_city">City</label></dt>
		<dd><input type="text" class="tfi" name="basic_city" value="" id="basic_city" required="required" /></dd>
		<dt><label for="basic_state">State</label></dt>
		<dd><select name="basic_state" value="" id="basic_state" required="required" ></dd>
			<option value="">Select State</option>
			<option value="NA">Not in USA</option>
			<option value="AL">Alabama</option>
			<option value="AK">Alaska</option>
			<option value="AZ">Arizona</option>
			<option value="AR">Arkansas</option>
			<option value="CA">California</option>
			<option value="CO">Colorado</option>
			<option value="CT">Connecticut</option>
			<option value="DE">Delaware</option>
			<option value="DC">District of Columbia</option>
			<option value="FL">Florida</option>
			<option value="GA">Georgia</option>
			<option value="HI">Hawaii</option>
			<option value="ID">Idaho</option>
			<option value="IL">Illinois</option>
			<option value="IN">Indiana</option>
			<option value="IA">Iowa</option>
			<option value="KS">Kansas</option>
			<option value="KY">Kentucky</option>
			<option value="LA">Louisiana</option>
			<option value="ME">Maine</option>
			<option value="MD">Maryland</option>
			<option value="MA">Massachusetts</option>
			<option value="MI">Michigan</option>
			<option value="MN">Minnesota</option>
			<option value="MS">Mississippi</option>
			<option value="MO">Missouri</option>
			<option value="MT">Montana</option>
			<option value="NE">Nebraska</option>
			<option value="NV">Nevada</option>
			<option value="NH">New Hampshire</option>
			<option value="NJ">New Jersey</option>
			<option value="NM">New Mexico</option>
			<option value="NY">New York</option>
			<option value="NC">North Carolina</option>
			<option value="ND">North Dakota</option>
			<option value="OH">Ohio</option>
			<option value="OK">Oklahoma</option>
			<option value="OR">Oregon</option>
			<option value="PA">Pennsylvania</option>
			<option value="RI">Rhode Island</option>
			<option value="SC">South Carolina</option>
			<option value="SD">South Dakota</option>
			<option value="TN">Tennessee</option>
			<option value="TX">Texas</option>
			<option value="UT">Utah</option>
			<option value="VT">Vermont</option>
			<option value="VA">Virginia</option>
			<option value="WA">Washington</option>
			<option value="WV">West Virginia</option>
			<option value="WI">Wisconsin</option>
			<option value="WY">Wyoming</option>
		</select>
		<dt><label for="basic_zip">ZIP Code</label></dt>
		<input type="text" class="tfi" name="basic_zip" value="" id="basic_zip" /></dd>
		<dt><p>Contact Method</p></dt>
		<dd><input type="radio" name="basic_pref" value="email" id="basic_pref_email" />
		<label for="basic_pref_email">Email</label></dd>
		<dd><input type="radio" name="basic_pref" value="phone" id="basic_pref_phone" />
		<label for="basic_pref_phone">Phone</label></dd>
		<dd><input type="radio" name="basic_pref" value="mail" id="basic_pref_mail" />
		<label for="basic_pref_mail">Mail</label></dd></dl>
	</fieldset>
	<fieldset id="basic_project">
		<dl><legend>Project Information</legend>
		<dt><label for="site1">Three websites you think fits your style</label></dt>
		<dd><input type="url" class="tfi" name="site1" placeholder="http://www.somepage.com" /></dd>
		<dt><label for="site2">Second Site</label></dt>
		<dd><input type="url" class="tfi" name="site2" placeholder="http://www.somepage.com" /></dd>
		<dt><label for="site3">Third Site</label></dt>
		<dd><input type="url" class="tfi" name="site3" placeholder="http://www.somepage.com" /></dd>
		<p>Services needed from CbyJ</p>
		
		<dd><input type="checkbox" name="services_needed" value="service_full" id="service_full">
		<label for="service_full">Full Package</label></dd>
		<dd><input type="checkbox" name="services_needed" value="service_art" id="service_art">
		<label for="service_art">Art</label></dd>
		<dd><input type="checkbox" name="services_needed" value="service_branding" id="service_branding">
		<label for="service_branding">Branding</label></dd>
		<dd><input type="checkbox" name="services_needed" value="service_content" id="service_content">
		<label for="service_content">Body Content</label></dd>
		<dd><input type="checkbox" name="services_needed" value="service_seo" id="service_seo">
		<label for="service_seo">Search Engine Optimization</label></dd>
		<dd><input type="checkbox" name="services_needed" value="service_uxo" id="service_uxo">
		<label for="service_uxo">User Experience Optimization</label></dd>
		<dd><input type="checkbox" name="services_needed" value="service_app" id="service_app">
		<label for="service_app">Application Creation</label></dd>
		<dd><input type="checkbox" name="services_needed" value="service_server" id="service_server">
		<label for="service_server">Server and Database Creation</label></dd>
		<dd><input type="checkbox" name="services_needed" value="service_server_opt" id="service_server_opt">
		<label for="service_server_opt">Server and Database Optimization</label></dd>
		<dd><input type="checkbox" name="services_needed" value="service_hosting" id="service_hosting">
		<label for="service_Hosting">Hosting</label></dd>
		<dd><input type="checkbox" name="services_needed" value="service_maint" id="service_maint">
		<label for="service_maint">Maintenance</label></dd>

		<dt><label for="deadline">Project Deadline</label></dt>
		<dd><input type="date" class="tfi" name="deadline" value="" id="deadline" placeholder="mm/dd/yyyy"></dd>
		<dt><label for="budget">Project Budget</label></dt>
		<dd><input type="text" class="tfi" name="budget" value="$" id="budget" placeholder="Amount in USD" /></dd></dl>
	</fieldset>
	<!-- Controls -->
  		<div class="quotebutton">
  			<input id="quotesubmit" name="quotesubmit" type="submit" value="Send&nbsp;to&nbsp;Joe" />
  		</div>
</form>
</div> <!-- Quote Page -->